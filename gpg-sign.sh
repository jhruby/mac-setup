#!/bin/bash

EMAIL=${1:-"jhruby@meds.cz"}

echo "Installing tools..."
brew install gnupg pinentry-mac

echo "Generating ssh key..."
ssh-keygen -t ed25519 -C ${EMAIL} -q -f ~/.ssh/id_ed25519 -N ""
eval "$(ssh-agent -s)"
ssh-add -K ~/.ssh/id_ed25519
cat ~/.ssh/id_ed25519.pub

echo "Adding key to gpg..."
gpg --quick-generate-key ${EMAIL} rsa4096 - never
GPG_KEY="$(gpg --list-secret-keys --keyid-format LONG | grep rsa4096 | tail -n1 | cut -d' ' -f4 | cut -d'/' -f2)"
gpg --armor --export ${GPG_KEY}
git config --global user.signingkey ${GPG_KEY}
echo "pinentry-program $(which pinentry-mac)" >> ~/.gnupg/gpg-agent.conf
gpgconf --kill gpg-agent
gpg-agent --daemon
