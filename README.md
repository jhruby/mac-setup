# Mac setup

Install essential tools and apps while setting up a new mac

## macOS tweaks
Tweak macOS default settings to make the work more comfortable.

```
bash <(curl -Ls https://gitlab.com/jhruby/mac-setup/raw/master/setup.sh)
```

## Apps
Install homebrew, all essential command line tools and apps. This will also restore settings for atom, git and bash profile.

```
bash <(curl -Ls https://gitlab.com/jhruby/mac-setup/raw/master/install.sh)
```
