#!/bin/bash

# install gcc
if test ! $(which gcc); then
  echo "Installing command line developer tools..."
  xcode-select --install
fi

# install homebrew
if test ! $(which brew); then
  echo "Installing homebrew..."
  bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
fi

echo "Updating homebrew..."
brew update

# install dev tools from brew
brew tap homebrew/cask
brew tap homebrew/cask-fonts
brew tap homebrew/homebrew-cask-versions
brew tap adoptopenjdk/openjdk

tools=(
  artginzburg/tap/sudo-touchid
  docker
  docker-compose
  git
  mas #Mac App Store CLI
  nvm
  openconnect
  yarn
  zsh-completions
)

echo "Installing binaries..."
brew install ${tools[@]}

sudo brew services start sudo-touchid

utils=(
  adoptopenjdk13 # java
  font-fira-code
  qlimagesize
  qlmarkdown
  quicklook-json
)

echo "Installing utils..."
brew install --cask ${utils[@]}

# install apps from brew
apps=(
  adguard
  eqmac
  firefox
  google-chrome
  insomnia
  macmediakeyforwarder
  messenger
  microsoft-teams
  polymail
  sketch
  slack
  spotify
  virtualbox
  visual-studio-code
  vlc
  webtorrent
  zoom
)

echo "Installing apps..."
brew install --cask ${apps[@]}

echo "Cleaning homebrew..."
brew cleanup

# install npm packages
npm=(
  madge
  nodemon
  ts-node
  serve
  source-map-explorer
  yarn-deduplicate
)

echo "Installing global npm packages"
yarn global add ${npm[@]}

# install apps from appstore
appstore=(
  409201541  #pages
  409203825  #numbers
  409183694  #keynote
  #497799835 #xcode
  1462114288 #grammarly
  970246631  #disk cleaner
)
echo "Installing apps from App Store..."
mas install ${appstore[@]}

echo "Installing vscode sync"
code --install-extension Shan.code-settings-sync

